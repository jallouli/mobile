import React from 'react'
import request from '../utils/client.utils'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { values } from '../constants/api.const';
import { data } from '../utils/data';
//signUp
let user;
let reqtk;
export async function register(login){
    console.log(login)
    try {
       
        const response = await request({
            method: 'POST',
            url: 'auth/signup',
           // headers: { 'Access-Control-Allow-Origin': true
       // },
            
            data: login, 
        })
        console.log(response);
        //console.log("Bearer "+response.data.token);
        //await   AsyncStorage.setItem("token",("Bearer "+response.data.token));
        //await AsyncStorage.setItem("id",(response.data.id));

        alert("Success");
        
                return response
       
    } catch (error) {
        alert("Error");
        console.log(error);
        throw (error.response || error.message)
   
    }
}
export async function signin(login){
    try{
        console.log("************************************************************************************")
        console.log(login)
        await AsyncStorage.clear();

        const response = await request({
            method: 'POST',
            url: 'auth/signin',
            data: login, 
        }).then(async (value)=>{
        console.log("signin------- successful");
        console.log(value);
        reqtk="Bearer "+value.data.token;
        console.log(value.data.user)
        user=value.data.user;
        console.log(user)
        //console.log(id)
        alert("Success");
    });

  
                return response

    } catch (error) {
       // alert("Error")
        throw (error.response || error.message)
      
    }
}
export async function sendmessage(message){
    try{

        const response = await request({
            method: 'POST',
            url: 'messages/save',
            data: message, 
        })
        /*console.log("Bearer "+response.data.token);
        await AsyncStorage.setItem("token",JSON.stringify("Bearer "+response.data.token));
        alert("Success");*/
      
                return response

    } catch (error) {
        alert("Error")
        throw (error.response || error.message)
      
    }
}
export async function getmessage(senderid,recieverid){
    try{

        const response = await request({
            method: 'GET',
            url: `messages/get/${senderid}/${recieverid}`,
             
        })
        //console.log("Bearer "+response.data.token);
        await AsyncStorage.setItem("messages",JSON.stringify(response));
        //alert("Success");
      
                return response

    } catch (error) {
        alert("Error")
        throw (error.response || error.message)
      
    }
}

export async function getfriends(){
    try{
        let res=[];
        console.log(user)

        const response = await request({
            method: 'GET',
            url: `user/getfriends/${user.id}`,
            headers: { 'token': reqtk
 },
             
        }).then(async(value)=>{     res=value; console.log(value) 
    })
        //console.log("Bearer "+response.data.token);
        //alert("Success");
      
                return res;

    } catch (error) {
        alert("Error")
        throw (error.response || error.message)
      
    }
}