import React ,{useEffect}from 'react'
import Background from '../components/Background'
import Logo from '../components/Logo'
import Header from '../components/Header'
import Button from '../components/Button'
import Paragraph from '../components/Paragraph'
import Search from '../components/SearchBar'
import Listusers from '../components/List'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Flex } from 'native-base'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ScrollView, View} from 'react-native';
import { getfriends } from '../services/Auth.service';



/*
 <Background>
    
      <Header>Let’s start</Header>
      <Paragraph>
        Your amazing app starts here. Open you favorite code editor and start
        editing this project.
      </Paragraph>
      <Button
        mode="outlined"
        onPress={() =>
          navigation.reset({
            index: 0,
            routes: [{ name: 'StartScreen' }],
          })
        }
      >
        Logout
      </Button>
    </Background>
*/
export default function Dashboard({ navigation }) {

  const datatest = [{name:"Ahmed"},{name:"Achref"},{name:"Sana"},{name:"Khawla"}]

 useEffect(async () => {
  // await getfriends().then((v)=>{console.log(v)})

  
  }, []);

  return (
       
    <Background>
          
          <View>
     <Search/>
        {
        datatest.map((a) => {
        
          return(
            
          <>
          <ScrollView>
          <TouchableOpacity   onPress={()=>{   AsyncStorage.setItem("User",JSON.stringify(a));
            
            navigation.navigate(
                    'Chat',
                     " " ,
                    )}} >
          <Listusers name= {a.name}/>
          </TouchableOpacity>
          </ScrollView>
          </>)
        })
      }
     </View>
    
      
    </Background>
  )
}
