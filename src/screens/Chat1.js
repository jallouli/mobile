import React, { useCallback, useEffect,useRef,useLayoutEffect, useState} from 'react'
import { GiftedChat,Send  } from 'react-native-gifted-chat'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import {
  VStack,
  Input,
  Button,
  IconButton,
  Icon,
  Text,
  NativeBaseProvider,
  Center,
  Box,
  Divider,
  Heading,
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { View, StyleSheet} from 'react-native';
import { Audio } from 'expo-av';
//import { Record } from '.';
/*

function getData(audioFile, callback) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var data = event.target.result.split(',')
         , decodedImageData = btoa(data[1]);                    // the actual conversion of data from binary to base64 format
        callback(decodedImageData);        
    };
    reader.readAsDataURL(audioFile);
}
*/
const Chat = ({route, navigation }) =>  {



  // Refs for the audio
  const AudioRecorder = useRef(new Audio.Recording());
  const AudioPlayer = useRef(new Audio.Sound());

  // States for UI
  const [RecordedURI, SetRecordedURI] = useState('');
  const [AudioPermission, SetAudioPermission] = useState(false);
  const [IsRecording, SetIsRecording] = useState(false);
  const [IsPLaying, SetIsPLaying] = useState(false);

  // Initial Load to get the audio permission
  useEffect(() => {
    GetPermission();
  }, []);

  // Function to get the audio permission
  const GetPermission = async () => {
    const getAudioPerm = await Audio.requestPermissionsAsync();
    SetAudioPermission(getAudioPerm.granted);
  };

  // Function to start recording
  const StartRecording = async () => {
    try {
      // Check if user has given the permission to record
      if (AudioPermission === true) {
        try {
          // Prepare the Audio Recorder
          await AudioRecorder.current.prepareToRecordAsync(
            Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
          );

          // Start recording
          await AudioRecorder.current.startAsync();
          SetIsRecording(true);
        } catch (error) {
          console.log(error);
        }
      } else {
        // If user has not given the permission to record, then ask for permission
        GetPermission();
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Function to stop recording
  const StopRecording = async () => {
    try {
      // Stop recording
      await AudioRecorder.current.stopAndUnloadAsync();

      // Get the recorded URI here
      const result = AudioRecorder.current.getURI();
      if (result) {SetRecordedURI(result);
      console.log(result+"/n let s go")}

      // Reset the Audio Recorder
      AudioRecorder.current = new Audio.Recording();
      SetIsRecording(false);
    } catch (error) {
      console.log(error);
    }
  };

  // Function to play the recorded audio
  const PlayRecordedAudio = async () => {
    try {
      // Load the Recorded URI
      await AudioPlayer.current.loadAsync({ uri: RecordedURI }, {}, true);

      // Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();
    
      // Play if song is loaded successfully
      if (playerStatus.isLoaded) {
        if (playerStatus.isPlaying === false) {
          AudioPlayer.current.playAsync();
          SetIsPLaying(true);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Function to stop the playing audio
  const StopPlaying = async () => {
    try {
      //Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();

      // If song is playing then stop it
      if (playerStatus.isLoaded === true)
        await AudioPlayer.current.unloadAsync();

      SetIsPLaying(false);
    } catch (error) {
      console.log(error);
    }
  };

const{uuid} = route.params;
const data = AsyncStorage.getItem("User");
const [messages, setMessages] = useState([]);
const [name,setName]=useState('');
const [imageUrl, setImageUrl] = useState('https://www.scarabay.com/images/testimonial/userimg.png');
  useEffect(() => {

    //get the name and photo from back

        }, [])

data.then((result)=>{
  console.log(JSON.parse(result).name);
})
    const onSend = useCallback((messages = []) => {
    setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    const {
    _id,
    createdAt,  
    text,
    user,
    } = messages[0]
  
    },[])
    useLayoutEffect(() => {
        //read messages from back and set them in unsubscribe
        setMessages()
      /*  const unsubscribe = chatact.orderBy('createdAt', 'desc').onSnapshot(snapshot => setMessages(
        snapshot.docs.map(doc => ({
        _id: doc.data()._id,
        createdAt: doc.data().createdAt.toDate(),
        text: doc.data().text,
        user: doc.data().user,
        }))
        ));
        return unsubscribe;*/
        }, [])
    

  return (
    
    <GiftedChat
      placeholder={'Type your message'}
      messages={messages}
      onSend={messages => onSend(messages)}
      showAvatarForEveryMessage={true}

      user={{
        _id: 1,
        name: name,
        avatar: imageUrl
        }}
        renderSend={(props)=>{
      
          return(
          
            <>
             
             {IsPLaying?(<>
              <TouchableOpacity onPress={StopPlaying}>
              <NativeBaseProvider>
          <Icon
            m="3"
            mr="2"
            size="6"
            color="gray.400"
            as={<MaterialIcons name="pause" />} />
            </NativeBaseProvider>
            </TouchableOpacity></>):(
            <>
              <TouchableOpacity onPress={PlayRecordedAudio}>
              <NativeBaseProvider>
          <Icon
            m="3"
            mr="2"
            size="6"
            color="gray.400"
            as={<MaterialIcons name="mic" />} />
            </NativeBaseProvider>
            </TouchableOpacity>
            </>)
            }
            {IsRecording ? (<>
                <TouchableOpacity onPress={StopRecording}>
                <NativeBaseProvider>
            <Icon
              m="3"
              mr="2"
              size="6"
              color="gray.400"
              as={<MaterialIcons name="pause" />} />
              </NativeBaseProvider>
              </TouchableOpacity></>):(
              <>
                <TouchableOpacity onPress={StartRecording}>
                <NativeBaseProvider>
            <Icon
              m="3"
              mr="2"
              size="6"
              color="gray.400"
              as={<MaterialIcons name="mic" />} />
              </NativeBaseProvider>
              </TouchableOpacity>
              </>)
            }
           
              <Send {...props} label={'Send'} /></>
            
          )}} 
        
    />
    
  );
};


  export default Chat;