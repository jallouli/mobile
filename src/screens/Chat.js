import React, { useCallback, useEffect,useRef,useLayoutEffect, useState} from 'react'
import { GiftedChat,Send  } from 'react-native-gifted-chat'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import { TouchableOpacity } from 'react-native-gesture-handler';
import { View, StyleSheet,Button,Image} from 'react-native';
import { Audio } from 'expo-av';
//import { Record } from '.';
/*

function getData(audioFile, callback) {
    var reader = new FileReader();
    reader.onload = function(event) {
        var data = event.target.result.split(',')
         , decodedImageData = btoa(data[1]);                    // the actual conversion of data from binary to base64 format
        callback(decodedImageData);        
    };
    reader.readAsDataURL(audioFile);
}
*/
const Chat = ({route, navigation }) =>  {

  const[datatest,setDatatest] = useState ([])
  
  // Refs for the audio
  const AudioRecorder = useRef(new Audio.Recording());
  const AudioPlayer = useRef(new Audio.Sound());

  // States for UI
  const [RecordedURI, SetRecordedURI] = useState('');
  const [AudioPermission, SetAudioPermission] = useState(false);
  const [IsRecording, SetIsRecording] = useState(false);
  const [IsPLaying, SetIsPLaying] = useState(false);
  
  // Initial Load to get the audio permission
  useEffect(() => {
    GetPermission();
  }, []);

  // Function to get the audio permission
  const GetPermission = async () => {
    const getAudioPerm = await Audio.requestPermissionsAsync();
    SetAudioPermission(getAudioPerm.granted);
  };

  // Function to start recording
  const StartRecording = async () => {
    try {
      // Check if user has given the permission to record
      if (AudioPermission === true) {
        try {
          // Prepare the Audio Recorder
          await AudioRecorder.current.prepareToRecordAsync(
            Audio.RECORDING_OPTIONS_PRESET_HIGH_QUALITY
          );

          // Start recording
          await AudioRecorder.current.startAsync();
          SetIsRecording(true);
        } catch (error) {
          console.log(error);
        }
      } else {
        // If user has not given the permission to record, then ask for permission
        GetPermission();
      }
    } catch (error) {
      console.log(error);
    }
  };

  // Function to stop recording
  const StopRecording = async () => {
    try {
      // Stop recording
      await AudioRecorder.current.stopAndUnloadAsync();

      // Get the recorded URI here
      const result = AudioRecorder.current.getURI();
      if (result) {
      console.log(result+"/n let s go")}

      // Reset the Audio Recorder
      AudioRecorder.current = new Audio.Recording();
      setDatatest([...datatest,{id:Math.floor(1000 + Math.random() * 9000),data:result,sender:{id:1},reciever:{id:0} }]);
      //console.log(datatest.length);
      //console.log(datatest);
      SetIsRecording(false);
    } catch (error) {
      console.log(error);
    }
  };

  // Function to play the recorded audio
  const PlayRecordedAudio = async () => {
    try {
      // Load the Recorded URI
      await AudioPlayer.current.loadAsync({ uri: RecordedURI }, {}, true);

      // Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();
    
      // Play if song is loaded successfully
      if (playerStatus.isLoaded) {
        if (playerStatus.isPlaying === false) {
          AudioPlayer.current.playAsync();
          SetIsPLaying(true);
        }
      }
     // console.log(datatest.length);

    } catch (error) {
      console.log(error);
    }
  };

  // Function to stop the playing audio
  const StopPlaying = async () => {
    try {
      console.log('yo i m here')
      //Get Player Status
      const playerStatus = await AudioPlayer.current.getStatusAsync();

      // If song is playing then stop it
      if (playerStatus.isLoaded === true)
        await AudioPlayer.current.unloadAsync();

      SetIsPLaying(false);
    } catch (error) {
      console.log(error);
    }
  };

const{uuid} = route.params;
const data = AsyncStorage.getItem("User");
const [messages, setMessages] = useState([]);
const [name,setName]=useState('');
const [imageUrl, setImageUrl] = useState('https://www.scarabay.com/images/testimonial/userimg.png');
  useEffect(() => {

    //navigation.navigate("chat", "")
        }, [datatest])

data.then((result)=>{
  console.log(JSON.parse(result).name);
})
    /*const onSend = useCallback((messages = []) => {
   // setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
    const {
    _id,
    createdAt,  
    text,
    user,
    } = messages[0]
  
    },[])*/
    useLayoutEffect(() => {
        //read messages from back and set them in unsubscribe
        setMessages()
      /*  const unsubscribe = chatact.orderBy('createdAt', 'desc').onSnapshot(snapshot => setMessages(
        snapshot.docs.map(doc => ({
        _id: doc.data()._id,
        createdAt: doc.data().createdAt.toDate(),
        text: doc.data().text,
        user: doc.data().user,
        }))
        ));
        return unsubscribe;*/
        }, [])
    

  return (
    
    <View style={styles.container}>
            <>
            
             {datatest.map((a)=>{
               console.log(a);
               //////////////////////////////////////////////////////
               console.log('7keya/////////////////////////////////////////////////////////')
               return(<>
            
            <View style={styles.messagecontainer}>
              <Button
            title={IsPLaying ? 'Stop' : 'Play'}
            color={IsPLaying ? 'red' : 'green'}
            onPress={()=>{
              SetRecordedURI(a.data);
              IsPLaying ? StopPlaying() : PlayRecordedAudio()}}
            />
            </View>
            </>)})}
            
            {IsRecording ? (<>
                <TouchableOpacity onPress={StopRecording} >
                  
                <Image source={require('../assets/micsh.png')}style={styles.image} />
        
              </TouchableOpacity></>):(
              <>
                <TouchableOpacity onPress={StartRecording}>
                <Image source={require('../assets/mic.png')}style={styles.image}/>
                
              </TouchableOpacity>
              </> )
            }
           
            </>            
            </View>
    
  );
};
/* style={styles.Imagecontainer}*/
const styles = StyleSheet.create({
  image: {
    width:120,
    height:140,
    margin:20,
    alignSelf:'center',
    top:0
  },
  container: {
    flex:1,
    alignContent:'center'
  },
  messagecontainer:{ 
  position:'relative',
  width:'90%',
  position:'relative',
  bottom:8,
  height:50 ,
  marginTop:20,
  marginLeft:15},

  Imagecontainer:{
    height:150,
    position:'absolute',
    
    bottom:8,
    width:'90%',
    
  }
})

  export default Chat;