import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  AppRegistry,
} from 'react-native';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';
import {
  VStack,
  Input,
  IconButton,
  Icon,
  NativeBaseProvider,
  Center,
  Box,
  Divider,
  Heading,
} from 'native-base';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Voice from 'react-native-voice';
export default class VoiceNative extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recognized: '',
      started: '',
      results: [],
    };
Voice.onSpeechStart = this.onSpeechStart.bind(this);
    Voice.onSpeechRecognized = this.onSpeechRecognized.bind(this);
    Voice.onSpeechResults = this.onSpeechResults.bind(this);
  }
componentWillUnmount() {
    Voice.destroy().then(Voice.removeAllListeners);
  }
onSpeechStart(e) {
    this.setState({
      started: '√',
    });
  };
onSpeechRecognized(e) {
    this.setState({
      recognized: '√',
    });
  };
onSpeechResults(e) {
    this.setState({
      results: e.value,
    });
  }
async _startRecognition(e) {
    this.setState({
      recognized: '',
      started: '',
      results: [],
    });
    try {
      await Voice.start('en-US');
    } catch (e) {
      console.error(e);
    }
  }
render () {
    return (
      <View>
      
        {this.state.results.map((result, index) => <Text style={styles.transcript}> {result}</Text>
        )}
       <TouchableOpacity onPress={this._startRecognition.bind(this)}>
                <NativeBaseProvider>
            <Icon
              m="3"
              mr="2"
              size="6"
              color="gray.400"
              as={<MaterialIcons name="mic" />} />
              </NativeBaseProvider>
              </TouchableOpacity>
        
        
      </View>
    );
  }
}
const styles = StyleSheet.create({
  transcript: {
    textAlign: 'center',
    color: '#B0171F',
    marginBottom: 1,
    top: '400%',
  },
});
AppRegistry.registerComponent('VoiceNative', () => VoiceNative);