import React, { useEffect , useState } from "react"
import { Box, Center, NativeBaseProvider } from "native-base"
import { width, height, totalSize } from 'react-native-dimension';

export const List = (props) => {
 
   // const[name,setName]=useState("Ahmed");
  return (
   <Box
      bg={{
        linearGradient: {
          colors: ['lightBlue.300', 'violet.800'],
          start: [0, 0],
          end: [1, 0],
          
        },
      }}
      p="8"
      rounded="xl"
      _text={{
        fontSize: 'md',
        fontWeight: 'medium',
        color: 'warmGray.50',
        textAlign: 'center',
      }}
      width= {width(80)}
       
      
    >
        {props.props.name}
        </Box>
  )
}

const config = {
  dependencies: {
    'linear-gradient': require('expo-linear-gradient').LinearGradient
  }
}

export default function Listusers(props) {
  return (
    <NativeBaseProvider  config={config}>
      <Center flex={1} px="1">
        <List  props={props}/>
      </Center>
    </NativeBaseProvider>
  )
}